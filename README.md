# Seminario 4
 Para un correcto funcionamiento de la aplicación, será necesario ejecutar antes el comando ```npm install```. Esta aplicación muestra un fichero html a los clientes que se conectan. El backend se identifica como el módulo index.js.
## Parte 1 
 Las funcionalidades añadidas al chat original son las siguientes: En primer lugar, en el momento que un nuevo usuario se conecta el mensaje 'user connected' se muestra a todos los usuarios conectados al servidor. Esto se hace en el evento del servidor cuando este 
 recibe una nueva conexión. Se ejecuta la siguiente orden de la línea 11: ```io.emit('chat message', 'user connected');```. Además, se ha añadido el soporte de nicks con la creación de un diálogo que al cargar la página web, pregunta al 
 cliente su nick. Esto se ha conseguido con el commando ```while (nickname == null || nickname == "") {
    nickname = prompt("Please enter your nickname");
  }```. Este comando muestra el diálogo que pregunta por el nombre de usuario y lo recoge como variable. Por último, la funcionalidad de enviar el mensaje a todos los servidores menos al propio se hace mediante el comando ```socket.broadcast.emit('chat message', msg);```.

## Parte 2  
La funcionalidad de añadir el usuario que está escribiendo se alcanza modificando el archivo html para que detecte cuándo se está escribiendo sobre la caja de texto del mensaje. Cuando este evento se produce, este módulo envía un mensaje al servidor para que muestre a los demás clientes
quién está escribiendo.  Para ello, se añade la función check() al evento de la caja de texto de la siguiente forma: ```<input id="m" autocomplete="off" onkeyup="check(this)" /><button>Send</button>```. La función check comprueba si la longitud del
texto introducido es mayor que 0, y si es así, envía al servidor el mensaje ```socket.emit('typing', nickname);```. Esto es recibido por el servidor y propagado a los clientes con la siguiente función: ```  socket.broadcast.emit('typing', msg);```.
La funcionalidad de quién está online se consigue añadiendo los nuevos usuarios a una lista del archivo html, y eliminándolos cuando se desconectan. Para ello, cuando un nuevo usuario se conecta, envía un mensaje con su nick a los demás usuarios, quienes lo reciben y 
lo añaden a la lista de usuarios.