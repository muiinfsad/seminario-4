var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

app.get('/', function (req, res) {
  res.sendFile(__dirname + '/index.html');
});

io.on('connection', function (socket) {
  console.log('a user connected');
  io.emit('chat message', 'user connected');
   socket.on('new user', function(msg){
    socket.broadcast.emit('new user',msg);
    socket.emit('new user',msg);
  })
  socket.on('disconnect', function () {
    console.log('user disconnected');
  });
  socket.on('chat message', function (msg) {
    console.log('message: ' + msg);
    // io.emit('chat message', msg);
    socket.broadcast.emit('chat message', msg);
  });
  socket.on('typing', function (msg) {
    console.log("typing: " + msg);
    
    socket.broadcast.emit('typing', msg);
  });
});

http.listen(3000, function () {
  console.log('listening on *:3000');
});
